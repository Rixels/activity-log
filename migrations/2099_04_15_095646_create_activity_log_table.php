 <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->unsignedInteger('executor_id')->nullable();
            $table->foreign('executor_id')->references('id')->on('users');

            $table->morphs('target');

            $table->string('message_type', 50);

            $table->json('extra_information')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
