<?php

return [

    /*
    * If set to false, no activities will be saved to a file.
    */
    'enable_file_logger' => env('ACTIVITY_FILE_LOGGER_ENABLED', false),

    /*
     * If set to false, no activities will be saved to the database.
     */
    'enable_database_logger' => env('ACTIVITY_DATABASE_LOGGER_ENABLED', true),

    /*
     * Name of the activity log table.
     */
    'table_name' => 'activity_logs',

    /*
     * Activity log model namespace.
     */
    'activity_log_model' => Rixels\ActivityLog\Models\ActivityLog::class,

    'executor_model' => env('ACTIVITY_EXECUTOR_MODEL', 'App\Models\User'),
];
