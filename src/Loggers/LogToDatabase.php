<?php namespace Rixels\ActivityLog\Loggers;

use Rixels\ActivityLog\Models\ActivityLog;
use Rixels\ActivityLog\Loggers\LogToInterface;

class LogToDatabase implements LogToInterface {


    public function log(array $data): bool
    {

        $activityLog = new ActivityLog();
        $activityLog->executor_id = $data['executor_id'];
        $activityLog->target_type = $data['target_type'];
        $activityLog->target_id = $data['target_id'];
        $activityLog->message_type = $data['message_type'];
        $activityLog->extra_information = (isset($data['extra_information']) ? $data['extra_information'] : null);

        return $activityLog->save();
    }
}
