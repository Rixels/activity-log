<?php namespace Rixels\ActivityLog\Loggers;

interface LogToInterface {

    public function log(array $data) : bool;
}
