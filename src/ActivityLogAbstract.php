<?php namespace Rixels\ActivityLog;

use Rixels\ActivityLog\Loggers\LogToDatabase;
use Rixels\ActivityLog\Loggers\LogToFile;

abstract class ActivityLogAbstract
{

    protected array $data;

    abstract function targetType(): string;

    public function __construct() {
        $this->data = [];
        $this->data['target_type'] = $this->targetType();
    }

    /**
     * @param int $executorId
     * @return self
     */
    public function executorId(?int $executorId = null): self {

        $this->data['executor_id'] = $executorId;

        return $this;
    }

    /**
     * @return self
     */
    public function targetId(int $targetId): self {

        $this->data['target_id'] = $targetId;

        return $this;
    }

    /**
     * @return void
     */
    public function logToDatabase(): void {

        //If database logger is enabled, proceed.
        if (config('activitylog.enable_database_logger')) {

            //Create instance
            $databaseLogger = new LogToDatabase();

            //Store log in database
            $databaseLogger->log($this->data);
        }
    }

    /**
     * @return void
     */
    public function logToFile(): void {

        //If file logger is enabled, proceed.
        if (config('activitylog.enable_file_logger')) {

            //Create instance
            $fileLogger = new LogToFile();

            //Store log in file
            $fileLogger->log($this->data);
        }
    }
}
