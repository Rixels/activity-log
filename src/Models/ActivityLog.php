<?php

namespace Rixels\ActivityLog\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{

    protected $table = 'activity_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['executor_id', 'target_type', 'target_id', 'message_type', 'extra_information'];

    public function executor() {
        return $this->belongsTo(config('activitylog.executor_model'), 'executor_id', 'id');
    }

    public function target() {
        return $this->morphTo('target')->withTrashed();
    }
}
