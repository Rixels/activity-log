<?php

namespace Rixels\ActivityLog\Traits;

trait LogTrait
{

    public function logCreatedMessage(): self {

        $this->data['message_type'] = 'created';

        return $this;
    }

    public function logAddedMessage(): self {

        $this->data['message_type'] = 'added';

        return $this;
    }

    public function logEditedMessage(array $originalData, array $changesData): self {
        $this->data['message_type'] = 'edited';

        if(count($originalData)) {
            unset($changesData['updated_at']);

            $this->setChanges($originalData, $changesData);
        }

        return $this;
    }

    public function logDeletedMessage(): self {
        $this->data['message_type'] = 'deleted';

        return $this;
    }

    public function logArchivedMessage(): self {
        $this->data['message_type'] = 'archived';

        return $this;
    }


    public function logRestoredMessage(): self {

        $this->data['message_type'] = 'restored';

        return $this;
    }

    private function setChanges(array $originalData, array $changesData) : void {

        $extraInformation = collect(['edited_fields_history']);

        foreach($changesData as $changeData => $key) {

            $extraInformation->put('edited_fields_history', ['field_name' => $changeData, 'old' => $originalData[$changeData], 'new' => $key]);

        }

        $this->data['extra_information'] = json_encode($extraInformation);
    }

}
